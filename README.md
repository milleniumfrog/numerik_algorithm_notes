# Numerik Vorlesung Algorithmen in Python

In diesem Repository sind in einem Jupyter Notebook die Algorithmen aus der Vorlesung nachimplementiert
mit kurzen Kommentaren und ein paar Graphen und anpassbaren Inputs.

## Installation

Um das Notebook nutzen zu können müssen Dependencies installiert werden.

- numpy
- scipy
- matplotlib
